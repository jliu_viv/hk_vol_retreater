from vivpy.kafka.kafka_stream import KafkaStream
from time import sleep
from math import exp
from theo_consumer import TheoConsumer

VOL_RETREAT = 0.1
MULTIPLIER = 50
# Square root vega or normal vega?
RETREAT_RATE = {
    0.95: 0,
    0.85: 0,
    0.7: 0,
    0.5: 50,
    0.3: 0,
    0.15: 0,
    0.05: 0,
    0.02: 0
}

DELTA_STD_DEV = {
    0.95: 0.02,
    0.85: 0.05,
    0.7: 0.01,
    0.5: 0.15,
    0.3: 0.1,
    0.15: 0.05,
    0.05: 0.05,
    0.02: 0.02
}

class VolRetreater():
    def __init__(self):
        self.trade_consumer = KafkaStream("HKFE_APP", run_unsafe=True)
        self.trade_consumer.subscribe(["HKFE_TRADE_FEED"])
        
        self.theo_consumer = TheoConsumer()
        self.theos = self.theo_consumer.theos

    @staticmethod
    def gaussian(b, c, x):
        return exp(-(x-b)**2/(2*c**2)) 
    
    @staticmethod
    def delta_weighted_vega(d_point, trade_delta, trade_vega):
        c = DELTA_STD_DEV[delta]
        return VolRetreater.gaussian(d_point, c, trade_delta)*trade_vega

    @staticmethod
    def call_delta(delta):
        if delta > 0:
            return delta
        else:
            return delta + 1
    
    def retreat_params(self, trade_delta, trade_vega):
        for d_point, retreat in RETREAT_RATE.items():
            weighted_vega = VolRetreater.delta_weighted_vega(d_point, trade_delta)*trade_vega
            vol_retreat = weighted_vega / retreat 
    
    def process_trades(self, trades):
        for trade in trades["TRADE_FEED"]:
            feedcode = trade["feedcode"]
            vega = trade["volume"] * self.theos[feedcode]["vega"]*MULTIPLIER
            delta = call_delta(self.theos[feedcode][delta]

            retreat_params(vega, delta)    

    def run(self):
        while True:
            trades = self.trade_consumer.get(fetch_all=True)

            self.process_trades(trade)
            sleep(2)

vr = VolRetreater()
print(vr.theo_consumer.theos)


