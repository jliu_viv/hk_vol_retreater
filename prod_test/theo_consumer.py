from vivpy.kafka.kafka_stream import KafkaStream
from datetime import datetime
from time import sleep
from threading import Thread
UPDATE_INTERVAL = 1


class TheoConsumer():
    def __init__(self):
        self.consumer = KafkaStream("HKFE_APP", run_unsafe=True)
        self.consumer.subscribe(["HKFE_GREEKS_FEED"])
        self.consumer.reset_to(datetime.today())

        self.theos = {}
        
        # Initial update_theos call is blocking
        self.update_theos()

        # Start thread to update theos in background
        self.thread = Thread(target=self.run, daemon=True)
        self.thread.start()

    def process_msg(self, msg):
        underlying = msg["chain"]

        for expiry in msg["expiry"]:
            self.process_expiry(expiry, underlying)

    def process_expiry(self, expiry, underlying):
        expiry_dt = datetime.strptime(str(expiry["date"]), "%Y%m%d")
 
        for strike in expiry["strike"]:
            for option in ["call", "put"]:
                if option in strike:
                    feedcode = TheoConsumer.get_feedcode(expiry_dt, underlying, option, strike["strike"])
                    self.theos[feedcode] = strike[option]

    @staticmethod            
    def get_feedcode(expiry_dt, underlying, option, strike):
        year = expiry_dt.year
        month = expiry_dt.month

        if option == "call":
            suffix = chr(month + 64) + str(year % 10)
        elif option == "put":
            suffix = chr(month + 64) + str(year % 10)

        return underlying + str(int(strike)) + suffix
        
    
    def update_theos(self):
        updates = self.consumer.get(fetch_all=True)

        for msg in updates["GREEKS_FEED"]:
            self.process_msg(msg)

    def run(self):
        while True:
            self.update_theos()

            sleep(UPDATE_INTERVAL)


