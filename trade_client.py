from pyviv import ClientSession
from base import header_pb2
from market.exec_messages_pb2 import TradeFeed
from market.simple_types_pb2 import OrderSide
from math import log, sqrt
from scipy.stats import norm
import logging

class TradeClient():
    def __init__(self, vol_curves, instruments):
        self.vol_curves = vol_curves
        self.instruments = instruments
        self.exec_session = ClientSession("wasat", 20002, "exec_session", header_pb2.CLIENT_TYPE_DB_SINK)

    def calculate_greeks(self, feedcode):
        def d1(F, K, t, iv):
            return (log(F/K) + (iv**2/2)*t)/(iv*sqrt(t))

        instrument = self.instruments[feedcode]

        underlying = instrument["underlying"]
        strike = instrument["strike"]
        expiry = instrument["expiry"]

        forward, tte, iv = self.get_spline_data(underlying, strike, expiry)
        option_d1 = d1(forward, strike, tte, iv)
        delta = norm.cdf(option_d1)
        vega = forward*norm.pdf(option_d1)*sqrt(tte)/100

        return delta, vega

    def get_spline_data(self, underlying, strike, expiry):
        spline = self.vol_curves[underlying][expiry].spline_params

        forward = spline.ref_fwd
        tte = spline.time_to_expiry
        x = log(strike/forward)

        for seg in spline.segment:
            if x >= seg.x_min and x <= seg.x_max:
                iv = seg.c0 + x*seg.c1 + pow(x, 2)*seg.c2 + pow(x, 3)*seg.c3
        
        return forward, tte, iv

    def process_trade(self, trade):
        feedcode = trade.instr_id.feedcode
        delta, vega = self.calculate_greeks(feedcode)

        if trade.side == OrderSide.SELL_ORDER:
            trade.volume *= -1

        cash_vega = trade.volume * vega * 50 
        underlying = self.instruments[feedcode]["underlying"]
        expiry = self.instruments[feedcode]["expiry"]
        strike = self.instruments[feedcode]["strike"]
        return cash_vega, delta, underlying, expiry, strike

    def fetch_trades(self):
        while True:
            trade = TradeFeed()
            self.exec_session.recv_one(header_pb2.MSG_XL_TRADE_FEED, trade)
            if trade.instr_id.feedcode in self.instruments:
                yield self.process_trade(trade)

