import math
from pricing.volcurve_pb2 import CubicSplineInputs
from service.peanut_pb2 import PeanutExpiryParams
from scipy.stats import norm
from scipy.interpolate import CubicSpline

def calc_parameter_risk(curve, strike, param):
    """
    Calculate the dvol/dparam using symmeterized numerical differentiation.
    Should return a number between 0 and 1 which is the parameter risk.
    """
    STEP_SIZE = 0.0005

    tte = curve.spline_params.time_to_expiry
    fwd = curve.spline_params.ref_fwd

    shift_up_inputs = shift_param(curve.spline_inputs, param, STEP_SIZE)
    shift_down_inputs = shift_param(curve.spline_inputs, param, -STEP_SIZE)
    
    shift_up_vol = calc_vol(shift_up_inputs, strike, tte, fwd)
    shift_down_vol = calc_vol(shift_down_inputs, strike, tte, fwd)
    return (shift_up_vol - shift_down_vol)/(2*STEP_SIZE)

    
def shift_param(spline_inputs, param, shift):
    """
    Vol model param shift. This should be reused in vol_client so that
    it is kept consistent.
    """
    inputs_copy = CubicSplineInputs()
    inputs_copy.CopyFrom(spline_inputs)
    points = inputs_copy.point

    if param == 0.5:
        for p in points:
            p.volatility += shift
    elif param > 0.5:
        for p in points:
            if p.delta >= param:
                p.volatility += shift
    elif param < 0.5:
        for p in points:
            if p.delta <= param:
                p.volatility += shift

    return inputs_copy 

def calc_vol(spline_inputs, strike, tte, fwd):
    """
    Draw a new spline with the inputs and calculate the new vol
    """
    x = []
    y = []

    for point in sorted(spline_inputs.point, key=lambda x: x.delta, reverse=True):
        point_strike = strike_from_delta(point.delta, point.volatility, tte, fwd)
        x.append(math.log(point_strike/fwd))
        y.append(point.volatility)

    new_spline = CubicSpline(x, y, bc_type="clamped")

    return new_spline(math.log(strike/fwd))
    
def strike_from_delta(delta, vol, tte, fwd):
    d1 = norm.ppf(delta)
    strike = 1/math.exp(d1*vol*math.sqrt(tte)-(vol**2)/2*tte)*fwd

    return strike

def get_vol(curve, strike):
    """
    Gets volatility from cubic spline
    """
    x = math.log(strike/curve.spline_params.ref_fwd)
    for segment in curve.spline_params.segment:
        vol = segment.c0 + segment.c1*x + segment.c2*x**2 + segment.c3*x**3

        if not segment.HasField("x_min") and x <= segment.x_max:
            return vol
        elif not segment.HasField("x_max") and x >= segment.x_min:
            return vol
        elif x >= segment.x_min and x <= segment.x_max:
            return vol

    return math.nan

if __name__ == "__main__":
    vol_points = {
        0.995: 45,
        0.98: 38,
        0.95: 32,
        0.85: 27,
        0.7: 24,
        0.5: 22,
        0.3: 21.5,
        0.15: 21.9,
        0.05: 23,
        0.02: 24,
        0.005: 25
    }
    
    tte = 5/256
    fwd = 24000

    curve = PeanutExpiryParams()
    curve.spline_params.time_to_expiry = tte
    curve.spline_params.ref_fwd = fwd

    for delta, vol in vol_points.items():
        point = curve.spline_inputs.point.add()
        point.delta = delta
        point.volatility = vol/100
        
        print(delta, strike_from_delta(point.delta, point.volatility, tte, fwd))

    print(calc_parameter_risk(curve, 23600, 0.3))
