from deapi.session import SyncDeapiSocket
from collections import defaultdict
from datetime import datetime
import logging
import pickle
import sys

USER = "vivtrader22"
ORC_SERVER = "arnie"
ORC_PORT = 9001

def process_instruments(underlyings):
    instruments = defaultdict(dict)
    instrument_list = fetch_instruments(underlyings)
    
    for instr in instrument_list:
        instr = instr.asDict()

        instr_id = instr["instrument_id"].asDict()

        if instr_id["kind"] == "Call" or instr_id["kind"] == "Put": 
            feedcode = instr_id["feedcode"]
                        
            instruments[feedcode]["expiry"] = datetime.strptime(instr_id["expirydate"], "%Y-%m-%d").date()
            instruments[feedcode]["strike"] = instr_id["strikeprice"]
            instruments[feedcode]["kind"] = instr_id["kind"]
            instruments[feedcode]["underlying"] = instr_id["underlying"]

    return instruments

def save_instruments(underlyings):
    instruments = process_instruments(underlyings)
    
    with open("instruments.pkl", "wb") as f:
        pickle.dump(instruments, f)

def load_instruments():
    with open("instruments.pkl", "rb") as f:
        instruments = pickle.load(f)

    return instruments

def fetch_instruments(underlyings):
    """
    Fetch instruments from Orc
    """

    instrument_list = []

    session = SyncDeapiSocket()
    try:
        session.connect((ORC_SERVER, ORC_PORT))
        logging.info(f"Connected to Orc {ORC_SERVER}:{ORC_PORT}")

        session.login(USER, USER)
        logging.info(f"Successfully logged in as {USER}")
        
        # The replies are split up over multiple messages so all the instruments are not
        # downloaded in one go. 
        for underlying in underlyings:
            instrument_list.extend(session.instrument_download(market="RMP", underlying=underlying))
            
            logging.info(f"Fetched {underlying} instruments")

    finally:
        session.close()

    return instrument_list

if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    save_instruments(["HSI", "HHI"])
