from instrument_download import process_instruments, load_instruments
from vol_client import VolClient
from trade_client import TradeClient
from vol_model import calc_parameter_risk
from datetime import date
import logging
import sys
import pandas as pd

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

VEGA_AMT = 100000

# Retreat rate vol_point/VEGA_AMT
RETREATS = {
    0.995: 0.5,
    0.98: 0.5,
    0.95: 0.5,
    0.85: 0.5,
    0.7: 0.5,
    0.5: 0.5,
    0.3: 0.5,
    0.15: 0.5,
    0.05: 0.5,
    0.02: 0.5,
    0.005: 0.5,
}

class AutoRetreater():
    def __init__(self):
        self.instruments = load_instruments() 

        self.vol_curves = {}

        self.vol_client = VolClient(self.vol_curves)
        self.trade_client = TradeClient(self.vol_curves, self.instruments)
        self.parse_retreat_params(RETREAT_FILE)
    
    def parse_retreat_params(self, filename):
        self.retreat_df = pd.read_csv(filename, index_col=0)
        self.retreat_df.columns = self.retreat_df.columns.astype(float)
        logging.info(f"Parsed retreat params from {RETREAT_FILE}")

    def run(self):
        """
        Fetch trades and retreat off the trades
        """
        for trade in self.trade_client.fetch_trades():
            c_vega, delta, underlying, expiry, strike = trade
            self.retreat(trade)
     

    def retreat(self, trade):
        """
        Calculate the parameter risk of a specific strike
        Should return a number between 0 and 1 which is the vega multiplier 
        """
        c_vega, delta, underlying, expiry, strike = trade
        curve = self.vol_curves[underlying][expiry] 

        for param, retreat_rate in RETREATS.items():
            vega_mult = calc_parameter_risk(curve, strike, param)
            adj_vega = vega_mult*c_vega
            
            # Divide by 100 to convert from vol point to raw vol point
            retreat = -1*adj_vega/VEGA_AMT*retreat_rate/100
            self.vol_client.shift_point(underlying, expiry, param, retreat)
            logging.info(f"Retreated {param} by {retreat*100}")

        self.vol_client.send_updates()
        
AutoRetreater().parse_retreat_params(RETREAT_FILE)
AutoRetreater().run()
