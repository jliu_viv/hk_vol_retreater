# HK Vol Retreater Prototype

This is a package of scripts for prototyping an auto retreater based on internal trades
in the simulation environment. 

## Requirements
Install deapy by cloning the repository from Bitbucket and running the below ocmmand
when you are in the folder.

```bash
pip install .
```

You will also need to clone the viv repo and generate protobufs in viv/protocols and add
viv/scripts to your $PYTHONPATH so you can access pyviv.py.

I recommend using a virtual env and adding these lines to the activate script.

```bash
export OLD_PYTHONPATH="$PYTHONPATH"
export PYTHONPATH="/home/jason.liu/viv/scripts/pb:/home/jason.liu/viv/scripts"
```

You will also need to add this line into the deactivate function.

```bash
export PYTHONPATH="$OLDPYTHONPATH"
```

The main entry point is autoretreater.py which runs on a loop to constantly receive trades and pushes changes
to vol spline.


