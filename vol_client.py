from pyviv import ClientSession
from service.peanut_pb2 import PeanutExpiryParamsSubscribe, PeanutExpiryParamsFeed
from pricing.volcurve_pb2 import VolatilitySplineUpdate, VolatilitySplineUpdateReply
from base import header_pb2
from base import base_types_pb2
from datetime import date
import logging
import sys
import threading


class VolClient():
    def __init__(self, vol_curves, name="vol_link"):
        self.peanut_session = ClientSession("wasat", 20050, name)
        self.vss_session = ClientSession("wasat", 20030, name)
        
        self.vol_curves = vol_curves
        self.subscribe_peanut()
        self.thread = threading.Thread(target=self.peanut_loop, daemon=True)
        self.thread.start()

        self.updates = VolatilitySplineUpdate() 
    
    
    def proto_to_date(expiry):
        """
        Converts protobuf Date message to Python date object
        """
        return date(expiry.year, expiry.month, expiry.day)
    
    def date_to_proto(expiry):
        """
        Convert Python date object to protobuf Date message
        """
        return base_types_pb2.Date(year=expiry.year, month=expiry.month, day=expiry.day)
        
    def process_expiry_feed(self, msg):
        for expiry_param in msg.expiry_params:
            underlying = expiry_param.underlying
            expiry_dt = VolClient.proto_to_date(expiry_param.expiry)
            
            if underlying not in self.vol_curves:
                self.vol_curves[underlying] = {}

            self.vol_curves[underlying][expiry_dt] = expiry_param
    
    def fetch_surface_update(self, underlying):
        for surf_update in self.updates.surface:
            if surf_update.underlying == underlying:
                return surf_update

        surf_update = self.updates.surface.add()
        surf_update.underlying = underlying

        return surf_update
    
    def fetch_curve_update(self, surf_update, underlying, expiry):
        """
        expiry (base_types_pb2.Date)
        """
        for curve in surf_update.curve:
            if curve.expiry == expiry:
                return curve

        new_curve = surf_update.curve.add()
        new_curve.expiry.CopyFrom(expiry)
        new_curve.cubic.CopyFrom(self.vol_curves[underlying][VolClient.proto_to_date(expiry)].spline_inputs)
        new_curve.cubic.revision += 1

        return new_curve

    def shift_point(self, underlying, expiry, delta, shift):
        surf_update = self.fetch_surface_update(underlying) 
        curv_update = self.fetch_curve_update(surf_update, underlying, VolClient.date_to_proto(expiry))
        
        if delta == 0.5:
            for point in curv_update.cubic.point:
                point.volatility += shift
        if delta > 0.5:
            for point in curv_update.cubic.point:
                if point.delta >= delta:
                    point.volatility += shift
        elif delta < 0.5:
            for point in curv_update.cubic.point:
                if point.delta <= delta:
                    point.volatility += shift


    def send_updates(self):
        self.vss_session.send(header_pb2.MSG_VSS_UPDATE, self.updates)
        self.updates = VolatilitySplineUpdate()

        reply = VolatilitySplineUpdateReply()
        self.vss_session.recv_one(header_pb2.MSG_VSS_UPDATE_REPLY, reply)

        # Need to handle replies and resend the diff if revision is rejected
        logging.info(f"Vol update reply: {VolatilitySplineUpdateReply.Status.Name(reply.status)})")

    def subscribe_peanut(self):
        """
        Subscribe to peanut and process snapshots
        """
        self.peanut_session.send(header_pb2.MSG_PDB_EXPIRY_PARAMS_SUBSCRIBE,
            PeanutExpiryParamsSubscribe(profile_name="HK"))

        msg_type = None

        while msg_type != header_pb2.MSG_PDB_SNAPSHOT_END:
            msg_type, msg_bytes = self.peanut_session.recv_any_non_heartbeat()

            if msg_type == header_pb2.MSG_PDB_EXPIRY_PARAMS_FEED:
                msg = PeanutExpiryParamsFeed()
                msg.ParseFromString(msg_bytes)
                self.process_expiry_feed(msg)

        logging.info("Finished processing peanut snapshots")

    def peanut_loop(self):
        while True:
            msg = PeanutExpiryParamsFeed()
            self.peanut_session.recv_one(header_pb2.MSG_PDB_EXPIRY_PARAMS_FEED, msg)
            self.process_expiry_feed(msg)
            
        
